# SmartBoard

Mise en place d’un dashboard (client) permettant de rédiger son projet via une interface intuitive. Ce dashboard a pour objectif de recenser les projets sur une même plateforme.

## Pour commencer

Pour bien intervenir sur le projet merci de regarder les dossiers sur le [Drive](https://docs.google.com/document/d/1kvMnnHYwc72sOW2s3vvOzvQ0pNPznAWQD_8GK8wJXRs/edit)

## Pré-requis

Merci d’installer :<br/>
       &nbsp; - [NPM 6.0.0](https://www.npmjs.com/)<br/>
       &nbsp; - [Terminal Git](https://www.atlassian.com/fr/git/tutorials/git-bash#:~:text=Git%20est%20un%20ensemble%20d,ligne%20de%20commande%20Unix%20int%C3%A9gr%C3%A9s.)

## Installation

Excécuter la commande: 
`Npm install`

## Démarrage

Pour lancer le projet il faut exécuter la commande : `Npm start` <br><br>
Lien [https://localhost:4200](https://localhost:4200/dashboard/cleint?2HRT0364KHGF/)


## Fabriqué avec

SmartBoard à était développé avec : 

- [ ] [Webstorm](https://docs.gitlab.com/ee/ci/quick_start/index.html) : Editeur de texte
- [ ] [React Version 17:0:0](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Jest Version 27.5](https://docs.gitlab.com/ee/topics/autodevops/requirements.html) : Pour les tests 
- [ ] [Axios Version 0.26.1](https://docs.gitlab.com/ee/topics/autodevops/requirements.html) : Pour les tests smoke
- [ ] [NodeJS Version 17.9.0 ](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [MySQL Version 8.0](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

## Versions

Dernière version stable : 5.0 Dernière version : 5.1 Liste des versions : [Cliquer pour afficher](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

## Auteurs

SmartAgence est l’auteur officiel de cette applications 

## License

Ce projet est sous licence : WTFTPL - voir le fichier [LICENSE.md](https://docs.gitlab.com/ee/ci/quick_start/index.html) pour plus d'informations

